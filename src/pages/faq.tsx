import * as React from 'react';
import { NextPage, GetStaticProps } from 'next';
import convert from 'htmr';
import styled from '@emotion/styled';

import { wp } from 'utils/api';
import { WordPressPost } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { Box } from 'components/ui-core';
import ErrorPage from 'pages/_error';
import { PostHeader } from 'modules/posts-index';
import htmrTransform from 'modules/posts-index/utils/htmrTransform';

interface PostContentPageProps {
  post?: WordPressPost;
  errors?: string;
}

const ContentAsSection = Content.withComponent('section');

const Section = styled(ContentAsSection)`
  padding-bottom: 48px;
`;

const FAQPage: NextPage<PostContentPageProps> = ({ post }) => {
  if (!post) {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <PageWrapper
      title={`${post.title.rendered} | KawalCOVID19`}
      description={post.excerpt.rendered}
    >
      <Box as="article" display="flex" flexDirection="column" flex="1 1 auto">
        <PostHeader
          type={post.type}
          title={post.title.rendered}
          description={post.excerpt.rendered}
        />
        <Section>
          <Column size="md">
            {convert(post.content.rendered, {
              transform: htmrTransform,
            })}
          </Column>
        </Section>
      </Box>
    </PageWrapper>
  );
};

export const getStaticProps: GetStaticProps = async () => {
  try {
    const post = await wp<WordPressPost>(`wp/v2/pages/277`);

    if (post && post.id) {
      const props = { post } as PostContentPageProps;
      return { props };
    }

    throw new Error('Failed to fetch page');
  } catch (err) {
    const props = { errors: err.message } as PostContentPageProps;
    return { props };
  }
};

export default FAQPage;
