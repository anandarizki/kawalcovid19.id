import * as React from 'react';
import { NextPage, GetStaticProps, GetStaticPaths } from 'next';
import styled from '@emotion/styled';
import convert from 'htmr';

import { wp } from 'utils/api';
import { WordPressPost, WordPressUser, WordPressCategory } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { Box } from 'components/ui-core';
import ErrorPage from 'pages/_error';
import { PostHeader, htmrTransform } from 'modules/posts-index';

interface PostContentPageProps {
  author?: WordPressUser;
  post?: WordPressPost;
  category?: WordPressCategory;
  errors?: string;
}

const ContentAsSection = Content.withComponent('section');

const Section = styled(ContentAsSection)`
  padding-bottom: 48px;
`;

const PostContentPage: NextPage<PostContentPageProps> = ({ post, author, category }) => {
  if (!post) {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <PageWrapper
      title={`${post.title.rendered} | KawalCOVID19`}
      author={author?.name}
      description={post.excerpt.rendered}
    >
      <Box as="article" display="flex" flexDirection="column" flex="1 1 auto">
        <PostHeader
          type={post.type}
          title={post.title.rendered}
          description={post.excerpt.rendered}
          author={author}
          category={category}
        />
        <Section>
          <Column size="md">
            {convert(post.content.rendered, {
              transform: htmrTransform,
            })}
          </Column>
        </Section>
      </Box>
    </PageWrapper>
  );
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    if (params) {
      const { paths } = params;
      const post = await wp<WordPressPost>(`wp/v2/posts/${paths[0]}`);

      if (post && post.id) {
        const author = await wp<WordPressUser>(`wp/v2/users/${post.author}`, {
          _fields: 'id,name,description,slug,avatar_urls',
        });

        const category = await wp<WordPressCategory>(`wp/v2/categories/${post.categories[0]}`);

        const props = {
          post,
          author: author.id ? author : undefined,
          category: category.id ? category : undefined,
        } as PostContentPageProps;
        return { props };
      }
    }

    throw new Error('Failed to fetch page');
  } catch (err) {
    const props = { errors: err.message } as PostContentPageProps;
    return { props };
  }
};

export const getStaticPaths: GetStaticPaths = async () => {
  const pagesList = await wp<WordPressPost[]>('wp/v2/posts', {
    per_page: 100,
  });

  const paths =
    pagesList && pagesList.length
      ? pagesList.map(page => {
          return { params: { paths: [`${page.id}`, `${page.slug}`] } };
        })
      : [];

  return {
    fallback: true,
    paths,
  };
};

export default PostContentPage;
