import * as React from 'react';
import styled from '@emotion/styled';
import { Portal, UnstyledButton, Box } from 'components/ui-core';
import { NavGrid, NavInner } from './NavComponents';
import CloseIcon from './CloseIcon';

export const SearchModalButton = styled(UnstyledButton)``;

interface SearchModalProps {
  isOpen?: boolean;
  onClose?: () => void;
}

const CloseButton = styled(UnstyledButton)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  width: 40px;
  height: 40px;
`;

const SearchModal: React.FC<SearchModalProps> = ({ isOpen, onClose }) => {
  const toggleModal = () => {
    if (onClose) {
      onClose();
    }
  };

  const renderInnerContent = () => {
    if (isOpen) {
      return (
        <Box
          display="flex"
          flexDirection="column"
          position="fixed"
          top={0}
          left={0}
          width="100%"
          height="100%"
          backgroundColor="background"
          color="foreground"
          zIndex={50}
        >
          <NavGrid backgroundColor="background" color="foreground">
            <NavInner display="flex" flexDirection="row">
              <Box
                display="flex"
                flexDirection="row"
                alignItems="center"
                flex="1 1 auto"
                height={60}
              >
                <input type="text" placeholder="Pencarian..." />
              </Box>
              <Box display="flex" flexDirection="row" alignItems="center" height={60}>
                <CloseButton type="button" backgroundColor="brandred" onClick={toggleModal}>
                  <CloseIcon />
                </CloseButton>
              </Box>
            </NavInner>
          </NavGrid>
          <NavGrid backgroundColor="background" color="foreground" flex="1 1 auto">
            <NavInner display="flex" flexDirection="column">
              SearchModal Content
            </NavInner>
          </NavGrid>
        </Box>
      );
    }

    return null;
  };

  return <Portal>{renderInnerContent()}</Portal>;
};

SearchModal.defaultProps = {
  isOpen: false,
  onClose: undefined,
};

export default SearchModal;
