import * as React from 'react';
import Link from 'next/link';
import { themeGet } from '@styled-system/theme-get';
import styled from '@emotion/styled';

import { Text, themeProps } from 'components/ui-core';
import { logEventClick } from 'utils/analytics';

interface NavLinkProps {
  title: string;
  href: string;
  as?: string;
  isActive?: boolean;
}

const TextLink = Text.withComponent('a');

const NavLinkRoot = styled(TextLink)<Pick<NavLinkProps, 'isActive'>>`
  display: flex;
  align-items: center;
  height: 48px;
  margin-right: 24px;
  text-decoration: none;
  border-bottom-width: 2px;
  border-bottom-style: solid;
  border-bottom-color: ${props =>
    props.isActive
      ? `${themeGet('colors.brandred', themeProps.colors.brandred)(props)}`
      : 'transparent'};
  white-space: nowrap;

  &:hover,
  &:focus {
    border-bottom-color: ${themeGet('colors.brandred', themeProps.colors.brandred)};
  }

  &:last-of-type {
    margin-right: 0;
  }
`;

const NavLink: React.FC<NavLinkProps> = ({ title, href, as, isActive }) => {
  return (
    <Link href={href} as={as} passHref>
      <NavLinkRoot isActive={isActive} onClick={() => logEventClick(title)}>
        {title}
      </NavLinkRoot>
    </Link>
  );
};

NavLink.defaultProps = {
  isActive: false,
};

export default NavLink;
