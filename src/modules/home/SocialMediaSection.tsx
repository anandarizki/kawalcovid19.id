import * as React from 'react';
import styled from '@emotion/styled';

import { Box, Heading, themeProps, UnstyledAnchor, Stack } from 'components/ui-core';
import { logEventClick } from 'utils/analytics';

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-columns: repeat(auto-fill, 1fr);
  grid-gap: 24px;

  ${themeProps.mediaQueries.md} {
    grid-template-columns: repeat(
      auto-fill,
      minmax(calc(${themeProps.widths.xl}px / 3 - 48px), 1fr)
    );
  }
`;

const SocialMediaSection: React.FC = () => {
  return (
    <Stack spacing="xl" mb="xxl">
      <Heading variant={800} as="h2">
        Ikuti kami di:
      </Heading>
      <GridWrapper>
        <UnstyledAnchor
          href="https://instagram.com/kawalcovid19.id"
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => logEventClick('Instagram')}
        >
          <Box bg="#D21C86" borderRadius={8}>
            <Box p="md">
              <Heading variant={600} as="h3" color="#fff">
                Instagram
              </Heading>
            </Box>
          </Box>
        </UnstyledAnchor>
        <UnstyledAnchor
          href="https://twitter.com/KawalCOVID19"
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => logEventClick('Twitter')}
        >
          <Box bg="#1da1f2" borderRadius={8}>
            <Box p="md">
              <Heading variant={600} as="h3" color="#fff">
                Twitter
              </Heading>
            </Box>
          </Box>
        </UnstyledAnchor>
        <UnstyledAnchor
          href="https://www.facebook.com/KawalCOVID19"
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => logEventClick('Facebook')}
        >
          <Box bg="#4267b2" borderRadius={8}>
            <Box p="md">
              <Heading variant={600} as="h3" color="#fff">
                Facebook
              </Heading>
            </Box>
          </Box>
        </UnstyledAnchor>
      </GridWrapper>
    </Stack>
  );
};

export default SocialMediaSection;
