import * as React from 'react';
import { Box, Text } from 'components/ui-core';
import { WordPressUser } from 'types/wp';
import styled from '@emotion/styled';
import Link from 'next/link';
import { logEventClick } from 'utils/analytics';

interface PostIndexAuthorProps {
  author: WordPressUser;
}

const IndexAvatar = styled('img')`
  width: 24px;
  height: 24px;
  object-fit: cover;
`;

const AuthorLink = styled(Text)`
  text-decoration: none;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const PostIndexAuthor: React.FC<PostIndexAuthorProps> = ({ author }) => {
  return (
    <Box display="flex" flexDirection="row" alignItems="center">
      <Box size={24} borderWidth={1} borderStyle="solid" borderRadius={24} overflow="hidden">
        <IndexAvatar alt={`${author.name}'s Avatar`} src={author.avatar_urls['24']} />
      </Box>
      <Link href="/author/[slug]" as={`/author/${author.slug}`} passHref>
        <AuthorLink as="a" variant={300} marginLeft="sm" onClick={() => logEventClick(author.name)}>
          {author.name}
        </AuthorLink>
      </Link>
    </Box>
  );
};

export default PostIndexAuthor;
