import * as React from 'react';
import styled from '@emotion/styled';
import convert from 'htmr';
import Link from 'next/link';

import { Heading, Paragraph, Box, Text } from 'components/ui-core';
import { Column, Content } from 'components/layout';
import { WordPressUser, WordPressCategory } from 'types/wp';
import { PostAuthor } from './components';

const Section = Content.withComponent('header');

const TextLink = Text.withComponent('a');

const CategoryLink = styled(TextLink)`
  text-decoration: none;
  text-transform: uppercase;

  &:hover,
  &:focus {
    text-decoration: underline;
  }
`;

const Root = styled(Section)`
  padding: 48px 24px 24px;
`;

interface PostHeaderProps {
  title: string;
  type?: string;
  description?: string;
  author?: WordPressUser;
  category?: WordPressCategory;
}

const PostHeader: React.FC<PostHeaderProps> = ({ title, description, type, author, category }) => {
  return (
    <Root noPadding noFlex>
      <Column size="md">
        {category && (
          <Box mb="xs">
            <Link href="/category/[slug]" as={`/category/${category.slug}`} passHref>
              <CategoryLink variant={400}>{category.name}</CategoryLink>
            </Link>
          </Box>
        )}
        <Box>
          <Heading as="h1" variant={900} maxWidth={640}>
            {convert(title)}
          </Heading>
          {type === 'post' &&
          category?.slug === 'informasi' && // hardcoded by slug
            description &&
            convert(description, {
              transform: {
                p: props => <Paragraph variant={500} mt="xs" maxWidth={640} {...props} />,
              },
            })}
        </Box>
        {type === 'post' && author && (
          <Box
            mt="md"
            pt="md"
            borderTopWidth="1px"
            borderTopStyle="solid"
            borderTopColor="accents03"
          >
            <PostAuthor author={author} />
          </Box>
        )}
      </Column>
    </Root>
  );
};

PostHeader.defaultProps = {
  type: 'page',
};

export default PostHeader;
