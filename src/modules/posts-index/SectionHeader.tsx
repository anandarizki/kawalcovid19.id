import * as React from 'react';
import styled from '@emotion/styled';
import { Heading, Paragraph } from 'components/ui-core';
import { Column, Content } from 'components/layout';

const Section = Content.withComponent('section');

const Root = styled(Section)`
  padding: 6vh 24px;
`;

interface SectionHeaderProps {
  title: string;
  description?: string;
}

const SectionHeader: React.FC<SectionHeaderProps> = ({ title, description }) => {
  return (
    <Root noPadding noFlex>
      <Column>
        <Heading as="h1" variant={900} maxWidth={640}>
          {title}
        </Heading>
        {description && (
          <Paragraph variant={500} mt="xs" maxWidth={640}>
            {description}
          </Paragraph>
        )}
      </Column>
    </Root>
  );
};

export default SectionHeader;
